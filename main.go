package main

import (
	"log"
	"sync"
	"time"
)

func main() {
	log.SetFlags(log.Lshortfile)
	log.Println("russian post is sending you a package...")
	log.Println("parcel has been shipped")

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		time.Sleep(10 * time.Second)
		wg.Done()
	}()

	go func() {
		for {
			time.Sleep(time.Second)
			log.Println("where the fuck is it??")
		}
	}()
	wg.Wait()
	log.Println("delivered!")
}
